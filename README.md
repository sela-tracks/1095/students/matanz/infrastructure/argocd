# ArgoCD installation

This README provides instructions on how to install an ArgoCD instance on Kubernetes:

(based on this page: https://argo-cd.readthedocs.io/en/stable/getting_started/)

**Requirements:**

* Installed kubectl command-line tool
* Connected to a Kubernetes cluster - Have a kubeconfig file (default location is /kube/config)

**Install Argo CD:**

1. Create a new namespace for Argo CD:

```
kubectl create namespace argocd
```

2. Apply the Argo CD manifest:
```
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```
This will create a new namespace, argocd, where Argo CD services and application resources will live.

Change the argocd-server service type to LoadBalancer:
```
kubectl patch svc argocd-server -n argocd -p '{"spec": "type": "LoadBalancer"}}'
```

4. Get the Argo CD password from the new terminal:
```
kubectl -n argocd get secret argood-initial-admin-secret -o jsonpath.date.password) | base64 -d
```

5. Access the Argo CD UI from your browser by typing the following URL:
```
http://<IP-address or server-name>
```
where the IP is the external-service-IP of the 'argocd-server' service.

Login to Argo CD UI using the username: 'admin' and and the above password
